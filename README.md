WHRC Member Demo
==================

This is a iOS(iPhone) project template of WHRC Member.

It is a eCommerce type application. Customer can order product and can do payment using authorize.net, brain tree and paypal from this application.
Customer can use their Credit Card for payment.  


It should look something like this:

![WHRC-Member-Demo](signin.png)

![WHRC-Member-Demo](signup.png)

![WHRC-Member-Demo](product_category.png)

![WHRC-Member-Demo](product_single.png)
