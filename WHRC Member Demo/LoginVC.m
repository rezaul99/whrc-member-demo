//
//  LoginVC.m
//  whrc
//
//  Created by Jijoty on 1/27/14.
//  Copyright (c) 2014 Shahnawaz. All rights reserved.
//

#import "LoginVC.h"
#import "User.h"
#import "WebServiceHelper.h"
#import "UserManager.h"
@interface LoginVC ()

@end

@implementation LoginVC
@synthesize btnLogin,btnSignup,txtEmail, txtPassword;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:_activityIndicator];
    _activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    
    self.lblWelcomeMessageTitle.text = @"Welcome to the WHRC POS System";
    self.lblWelcomeMessage.text = @"Our mission is to support women and their families in making informed decisions about their health and to encourage them to become active partners in their care. We are committed to providing free information, resources and referrals, and an empathetic ear regarding all women's health issues. Come visit us and enjoy our relaxing environment; browse our women's health library, use our patient computer, breastfeed your baby in comfort using our deluxe breastfeeding glider, or just gain knowledge on current women's health topics. We are proud to be the patient and community education arm of the UCSF National Center of Excellence in Women's Health. This program is one of 19 nationwide that has been designated by the US Office of Women's Health as a model of women's health care delivery.";
    self.vMainUI.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background-iPad.png"]];
    self.idBtnRegister.hidden = true;
}
-(IBAction) btnLogin : (id)sender
{
    User *userObj = [[User alloc] init];
    userObj.Email = self.txtEmail.text;
    userObj.Password = self.txtPassword.text;
    userObj.UserType = @"Admin";
    
    if(self.txtEmail.text != nil && self.txtPassword.text != nil && ![self.txtEmail.text isEqualToString:@""] && ![self.txtPassword.text isEqualToString:@""])
    {
        MiscellaniesManager *miscellaniesManager = [[MiscellaniesManager alloc]init];
        if([miscellaniesManager validateEmail:self.txtEmail.text])
        {
            webServiceHelper = [[WebServiceHelper alloc]init];
            webServiceHelper.delegate = self;
            if(miscellaniesManager.IsInternetConnectionAvaiable)
            {
                [_activityIndicator startAnimating];
                [webServiceHelper Login:userObj];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:@"Check internet connection\n\n" delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
                [alert show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:@"Email address is not valid.\n\n" delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:@"Enter email address or password.\n\n" delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
        [alert show];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(IBAction) btnSignup : (id)sender
{
    [self performSegueWithIdentifier:@"SignupVCSegue" sender:self];
}
-(void)GetDataSucceed:(NSMutableArray *)dataList:(NSString *)toolName
{   
    if(dataList.count == 1)
    {
        User *userObj = [dataList objectAtIndex:0];
        if([userObj.Result isEqualToString:@"true"])
        {
            userObj.Email = self.txtEmail.text;
            userObj.Password = self.txtPassword.text;
            
            // UserManager *userManager = [[UserManager alloc]init];
            // [userManager Save:userObj];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            // [defaults setObject:userObj.UserId forKey:@"UserId"];
            NSString *name = [NSString stringWithFormat:@"%@ %@",userObj.FirstName, userObj.LastName];
            [defaults setInteger:userObj.UserId forKey:@"AdminId"];
            [defaults setObject:name forKey:@"AdminName"];
            [defaults setObject:userObj.Email forKey:@"AdminEmail"];
            [defaults synchronize];
            
            //[self performSegueWithIdentifier:@"LoginSegue" sender:self];
           /* UINavigationController *leftNavController = (UINavigationController *)self.revealViewController.frontViewController;
            MasterVC *destViewController = (MasterVC* )leftNavController;
            
            UISplitViewController *svController = (UISplitViewController*)[[[UIApplication sharedApplication] keyWindow] rootViewController];
            MasterVC *cVC= (MasterVC *)[svController.viewControllers objectAtIndex:0];
            UINavigationController *navController = [svController.viewControllers objectAtIndex:0];
            SWRevealViewController *rVController = [navController.viewControllers objectAtIndex:0];
            MasterVC *viewController= (MasterVC* )(UINavigationController *)rVController.frontViewController;
            
            
            self.delegateCategory = viewController;
             if (_delegateCategory)
             {
             ProductCategory *name = [[ProductCategory alloc]init];
             name.CategoryName = @"Class";
             
             [_delegateCategory selectedCategory:name];
             }*/
            [self dismissModalViewControllerAnimated:YES];
        }
        else
        {
            NSString *message = [NSString stringWithFormat:@"%@ \n\n",userObj.Result];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:message delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
            [alert show];
        }
    }
    [_activityIndicator stopAnimating];
}
-(void)GetDataFailed:(NSString *)failMessage
{
    [_activityIndicator stopAnimating];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:failMessage delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
    [alert show];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
