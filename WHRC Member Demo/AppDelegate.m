//
//  AppDelegate.m
//  WHRC
//
//  Created by Jijoty on 3/17/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import "AppDelegate.h"
#import "whrcViewController.h"
#import "User.h"
#import "UserManager.h"
#import "LoginVC.h"
#import "ClassCVC.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
   // [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
   // [[UINavigationBar appearance] setBarTintColor:[UIColor redColor]];
    UserManager *userManager = [[UserManager alloc]init];
    User *userObj = [userManager Get:1];
    
    //NSString *deviceType = [UIDevice currentDevice].model;
    //if(![deviceType isEqualToString:@"iPhone Simulator"])
    
    if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad)
    {
        if(userObj != nil)
        {
            BOOL isLoggedIn = false;
            NSString *storyboardId = isLoggedIn ? @"LoginVC" : @"TabBarController";
        
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
            UIViewController *initViewController = [storyboard instantiateViewControllerWithIdentifier:storyboardId];
        
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            self.window.rootViewController = initViewController;
        }
        else
        {
            BOOL isLoggedIn = true;
            NSString *storyboardId = isLoggedIn ? @"LoginVC" : @"TabBarController";
        
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle: nil];
            UIViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:storyboardId];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:lvc];
            navigationController.navigationBar.hidden = true;
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            self.window.rootViewController = navigationController;
        }
    }
    else
    {
       // [[UITabBar appearance] setTintColor:[UIColor redColor]];
        if(userObj != nil)
        {
            BOOL isLoggedIn = false;
            NSString *storyboardId = isLoggedIn ? @"iLoginVC" : @"TBCStoryboardId";
        
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            UIViewController *initViewController = [storyboard instantiateViewControllerWithIdentifier:storyboardId];
        
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            self.window.rootViewController = initViewController;
        }
        else
        {
            BOOL isLoggedIn = true;
            NSString *storyboardId = isLoggedIn ? @"iLoginVC" : @"TBCStoryboardId";
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
            UIViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:storyboardId];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:lvc];
            navigationController.navigationBar.hidden = true;
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            self.window.rootViewController = navigationController;
        }
    }    
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
