//
//  LoginVC.h
//  whrc
//
//  Created by Jijoty on 1/27/14.
//  Copyright (c) 2014 Shahnawaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MiscellaniesManager.h"
#import "WebServiceHelper.h"
#import "SelectCategoryDelegate.h"
#import "SWRevealViewController.h"
#import "MasterVC.h"

@interface LoginVC : UIViewController<UITextFieldDelegate,WebServiceDelegate>
{
    UITextField *txtEmail;
    UITextField *txtPassword;
    WebServiceHelper *webServiceHelper;
}
@property (nonatomic) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, assign) id<SelectCategoryDelegate> delegateCategory;

@property(nonatomic,retain) IBOutlet UITextField *txtEmail;
@property(nonatomic,retain) IBOutlet UITextField *txtPassword;
@property (nonatomic, retain) IBOutlet UIButton *btnLogin;
@property (nonatomic, retain) IBOutlet UIButton *btnSignup;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcomeMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcomeMessageTitle;
@property (weak, nonatomic) IBOutlet UIButton *idBtnRegister;

@property (strong, nonatomic) IBOutlet UIView *vMainUI;

-(IBAction) btnLogin : (id)sender;
-(IBAction) btnSignup : (id)sender;
@end
